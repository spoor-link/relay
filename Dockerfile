FROM node:lts

RUN apt-get update && apt-get install -y \
    supervisor

WORKDIR /app

COPY package*.json ./

RUN npm ci --omit=dev

COPY . .

RUN chmod +x run.sh

EXPOSE 7664
EXPOSE 9200

# Copy supervisor configuration
COPY docker/supervisord.conf /etc/supervisord.conf

CMD ["/usr/bin/supervisord", "-n", "-c", "/etc/supervisord.conf"]