# Spoor.link Relay Server

![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/spoor-link/relay)
![Docker Pulls](https://img.shields.io/docker/pulls/spoorlink/relay)
![GitLab](https://img.shields.io/gitlab/license/spoor-link/relay)
![GitLab last commit](https://img.shields.io/gitlab/last-commit/spoor-link/relay)
![GitLab tag (latest by date)](https://img.shields.io/gitlab/v/tag/spoor-link/relay)

## [Gitlab repository](https://gitlab.com/spoor-link/relay)

The relay server is a ZeroMQ message broker that relays messages from 1 or more subscribers, to various publishers. This
started existing because the open data broker for Dutch public transport information (NDOV) allows a limited number of
connections with fixed IP addresses. To enable more connections, and development from areas with dynamic IP addresses,
this relay server was created.

This is the second generation relay server for spoor.link. The first generation (version 0) ran for a long time, but
was built on now-outdated software. and couldn't fulfill new wishes from us.

## Features

* **Hot reload**, as soon as you change the configuration file, it is applied, no need to restart the relay!
* **Many subscribers to many publishers routing**, do you want messages from many subscribers reachable on just one publisher, or the
  other way around? Anything is possible with message routing
* **Prometheus metrics**, the app will show you detailed metrics on the traffic coming through the relay
* **Fast and simple**, adding almost no delay to the messages, this relay can be used for realtime data. Real world delay was measured to be an average of 1.94ms, with 1000 messages a second

## Installation

This app can be run via local installation or Docker. Below are instructions to get you going quickly for both
solutions.

### Local installation

Requirements for running the app locally:

* Node.JS 12 or newer

Steps to install:

1. Download the project from Gitlab: `git clone git@gitlab.com:spoor-link/relay.git`
2. Go to the project folder, and install the node packages: `npm ci` (optionally add `--omit=dev` for production)
3. Copy `config.example.yml` to `config.yml`, and changes the file to match your setup (see #configuration for details)
4. Optionally, specify any environment variables (see #configuration for details)
5. Start the app from the file `app.js`

### Docker installation

Requirements for running the app on Docker:

* Docker-ce or better

The easiest way to get started, is to first copy `config.example.yml` to `config.yml`, and change the file to your
needs (see #configuration for details).

Next, you start the
container: `docker container run --name relay -v /path/to/your/config.yml:/app/config.yml spoorlink:latest`. This will
create a Docker container called "relay", with your config.yml file mounted to the app/ directory.

We recommend you also mount the /app/cache directory, for example by adding `-v /path/to/your/cache:/app/cache`.

## Accessing data

Every publisher you add to the configuration will be reachable on the port you specify. Apart form that, the app provides
Prometheus metrics, by default on port 9200. These can be reached by navigating to: `http://<ip-address>:9200/metrics`.

## Configuration

The main configuration of the app is located in its `config.yml` file. Some additional configuration can be set via
environment values. This chapter will explain both.

### config.yml

This is the main configuration file for the app. Here, the subscribers, publishers, and some settings for the metrics are
located. This projects has an example file, called `config.example.yml`, to get you started. You can copy this file
to `config.yml` and change the values according to your needs. Below, we will explain what the variables in each section
mean.

**Subscribers**

The subscribers listen to other ZeroMQ servers, with the goal of relaying these messages to the subscribers of this app. Each
subscriber has the following properties:

* `name`: The name of the subscriber. This is only used by the app to keep track of changes to subscriber. The name can contain
  alphanumeric characters
* `address`: The TCP address of the external publisher. This address must include the protocol, IP address/hostname, and
  port. Make sure there is no trailing slash (`/`) at the end of the address. For example, `tcp://vid.openov.nl:6703` is
  a
  valid entry
* `envelope`: The envelope of the messages you want to receive. This constrains the messages sent to the app from the
  external publisher. For most NDOV sources, a single slash (`/`) is common to receive all messages, but this depends on
  the publisher
* `publishers`: A list of all publishers messages received by this subscriber need to be sent to. If you want to send the messages
  to all publishers in the configuration file, you will need to add all of them to this list
* `minMessages`: The minimum amount of messages that should be received within the interval set below. If this message count is not reached,
  the subscriber will be restarted
* `minMessagesCheckInterval`: The interval in which the minimum amount of messages must be reached. Valid entries are: `second`, `minute`, `hour`

Below is an example of a valid entry in the `subscribers` section of the configuration:

```yaml
---
subscribers:
  - name: Fiets API
    address: "tcp://vid.openov.nl:6703"
    envelope: "/"
    publishers:
      - main
```

**Publishers**

The publishers forward (relay) messages from its subscribers in the app to external subscribers. Each publisher only sends messages
from subscribers it's connected to (this is set in the subscribers configuration). Each publisher has the following properties:

* `name`: The name of the publisher. This is only used by the app to keep track of changes to publishers. The name can contain
  alphanumeric characters
* `binding`: What address the publisher should bind on. This is typically to all outside connections, on a specific port.
  This must include the protocol, IP address, and port. Make sure there is no trailing slash (`/`) at the end of the
  address. For example, `tcp://0.0.0.0:7664` is a valid entry

Below is an example of a valid entry in the `publishers` section of the configuration:

```yaml
---
publishers:
  - name: main
    binding: "tcp://0.0.0.0:7664"
```

**Prometheus envelope mappers**

This section contains a set of mappers used by the Prometheus metrics. These are used to change the envelope used on the
output of the metrics. This can be useful to group some (or all) envelopes under a single label. For example, if you
want `/train/stationA`, and `/train/stationB` to be collected under the same label in the Prometheus metrics, you
provide the following mapper:

```yaml
---
prometheusEnvelopeMappers:
  - startsWith: "/train"
    output: "/train"
```

Here, the `startsWith` property defines what the mapper will match on. Of there is a match, the entire envelope will be
replaced by the value in the `output` property. This only changes the envelope in the Prometheus metrics, it doesn't
change the envelopes of the messages sent to external subscribers! Those cannot be changed.

### Environment variables

Below is a list of all environment variables available for the app. These can be set easily in the Docker configuration,
or can be set as regular environment variables in the system OS.

| Value                 | Required? | Default value | Description                                                                                                                                                                                                                                                               |
|-----------------------|-----------|---------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| CONFIG_FILE           | No        | ./config.yml  | The location of the configuration file                                                                                                                                                                                                                                    |
| CONFIG_CHECK_INTERVAL | No        | 60000         | How often the configuration file will be checked for changes, in milliseconds. These changes should be detected automatically, but in case that doesn't happen, the app also checks periodically.<br/>Set to 0 to disable. The minimum value while active is 1000 (1 sec) |
| METRICS_SAVE_INTERVAL | No        | 2000          | How often the metrics are saved to the cache, in milliseconds. This ensures the metrics are preserved on restart of the app<br/>Set to 0 to disable. The minimum value while active is 1000 (1 sec)                                                                       |
| WEB_PORT              | No        | 9200          | What port the web server uses for HTTP traffic                                                                                                                                                                                                                            |

## Testing messages

If you want to check if messages get through the relay successfully, this project includes a simple test script, located
at `test.js`. It connects to your relay as a subscriber, and shows each message it receives. To configure the test
script, set the following environment variables before running it:

* `ADDRESS`: The address of the ZeroMQ publisher (see #configuration for details on the syntax)
* `ENVELOPE`: The envelope for the messages

## Benchmarking relay

The relay provides a script `benchmark.js` to stress test the relay. It will send increasing loads to the relay, and check the delay of the messages coming back. This is the round trip delay of the messages, including: network delay to the relay, processing delay of the relay, network delay back to the benchmark script. It can send a maximum of 1000 messages a second.

You can create the file `bench/test.json` next to the benchmark script, and define the desired test pattern there as a JSON object. This is an array of tests, each containing a JSON object with the parameters `messagesPerSecond` and `testPeriodSeconds`. If this file is not provided, the benchmark will run a predefined test pattern. Below is an example of a valid `test.json` file:

```json
[
  {
    "messagesPerSecond": 1,
    "testPeriodSeconds": 30
  },
  {
    "messagesPerSecond": 100,
    "testPeriodSeconds": 30
  }
]
```

We recommend running the relay and benchmark script on two different machines on a local network, to test all aspects of the relay, including communicating to a network connection.

Set the following environment variables before running the benchmark:

* `PUBLISHER_ADDRESS`: The address the benchmark script will bind on to send messages. The relay should listen to this address (default 'tcp://0.0.0.0:7664')
* `SUBSCRIBER_ADDRESS`: The address for the subscriber to connect to the relay

## Reporting problems

This software is provided as-is, therefor there is no official support for problems while setting up and configuring the
software.

If you encounter bugs, go to the [Gitlab repository](https://gitlab.com/spoor-link/relay) and create a new issue. For
support questions, we will try our best to help out.