const zmq = require('zeromq');

let socket = zmq.socket('sub');
socket.connect(process.env.ADDRESS);
socket.subscribe(process.env.ENVELOPE);

socket.on('message', (envelope, message) => {
    envelope = envelope.toString();

    console.log(new Date().toISOString() + ' - ' + envelope);
});