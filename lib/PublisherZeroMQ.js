const zmq = require('zeromq');

/**
 * ZeroMQ Publisher.
 * This provides a ZeroMQ publisher external subscribers can connect with, to receive the messages received by this app.
 */
class PublisherZeroMQ {
    /**
     * @type string
     */
    name;

    /**
     * @type string
     */
    binding;

    zmq;

    constructor(name, binding) {
        this.name = name;
        this.binding = binding;
    }

    /**
     * Creates a ZeroMQ publisher
     */
    bind() {
        if (!this.zmq) {
            this.zmq = zmq.socket('pub');
            this.zmq.bindSync(this.binding);

            console.log('binding publisher ' + this.name);
        }
    }

    /**
     * Close the publisher
     */
    destroy() {
        if (this.zmq) {
            this.zmq.disconnect(this.binding);

            console.log('Disconnecting publisher ' + this.name);
        }
    }

    /**
     * Send a message to this publisher
     * @param {string} envelope The envelope of the message
     * @param {string} message The message to be sent to all subscribers
     */
    sendMessage(envelope, message) {
        try {
            this.zmq.send([envelope, message]);
        } catch (e) {
            console.error(e);
        }
    }
}

module.exports = PublisherZeroMQ;