const SubscriberManager = require('./SubscriberManager');
const PublisherManager = require('./PublisherManager');
const promClient = require('prom-client');
const collectDefaultMetrics = promClient.collectDefaultMetrics;
const Registry = promClient.Registry;
const express = require('express');
const webApp = express();
const promBundle = require("express-prom-bundle");
const metricsMiddleware = promBundle({includeMethod: true});
const fs = require('fs');

/**
 * Offers a simple webserver for the app, to show statistics.
 * Currently, the only feature this is used for, is to display Prometheus metrics for monitoring dashboards
 */
class Webserver {
    /**
     * @type SubscriberManager
     */
    subscriberManager;

    /**
     * @type PublisherManager
     */
    publisherManager

    /**
     * @type Registry
     */
    prometheus;

    /**
     * Stores the metric Counter instances
     * @type {Object.<string, Counter<string>|null>}
     */
    metrics = {};

    /**
     * All envelope mappers
     * @type {Object[]}
     */
    envelopeMapper = [];

    constructor(subscriberManager, publisherManager) {
        this.subscriberManager = subscriberManager;
        this.publisherManager = publisherManager;
    }

    /**
     * Initializes the webserver
     */
    run() {
        this._startServer();
        this._registerMetrics();

        let saveInterval = process.env.METRICS_SAVE_INTERVAL ? parseInt(process.env.METRICS_SAVE_INTERVAL) : 2000;

        if(saveInterval > 0) {
            setInterval(() => {
                this._saveMetrics();
            }, Math.max(1000, saveInterval));
        }
    }

    /**
     * Receives a config object, and processes this to store update envelope mappers for Prometheus
     * @param {Object[]} envelopeMappers The updated configuration to apply
     */
    parseConfig(envelopeMappers) {
        this.envelopeMapper = envelopeMappers;
    }

    /**
     * Start the webserver on it's designated port and start the Prometheus metrics
     * @protected
     */
    _startServer() {
        this.prometheus = new Registry();
        collectDefaultMetrics(this.prometheus);

        webApp.use(metricsMiddleware);

        webApp.listen(process.env.WEB_PORT ? parseInt(process.env.WEB_PORT) : 9200, () => {
            console.log('Started metrics webapp');
        });
    }

    /**
     * Register the application specific metrics on the Prometheus client
     * @protected
     */
    _registerMetrics() {
        this.metrics['app_messages_received'] = this._registerPreservedCounter({
            name: 'app_messages_received',
            help: 'app_messages_received',
            labelNames: ['address', 'envelope'],
        });

        this.subscriberManager.subscriberCallback.register((err, address, envelope) => {
            try {
                // Apply the envelope mappers to the envelope of a received message
                for (let i = 0; i < this.envelopeMapper.length; i++) {
                    let mapper = this.envelopeMapper[i];

                    // Replace the envelope if the start of the envelope matches a string
                    if (mapper.startsWith) {
                        if (envelope.startsWith(mapper.startsWith)) {
                            envelope = mapper.output;
                            break;
                        }
                    }
                }

                if (this.metrics['app_messages_received']) {
                    this.metrics['app_messages_received'].inc({
                        address,
                        envelope,
                    });
                }
            } catch (e) {
                console.error(e);
            }
        });
    }

    /**
     * Create a new counter metric, which allows to restore its counter values after a restart of the app
     * @param {Object} configuration The configuration for the counter
     * @return {Counter<string>|null}
     * @protected
     */
    _registerPreservedCounter(configuration) {
        let counter = null;

        try {
            counter = new promClient.Counter(configuration);

            if (fs.existsSync('cache/' + configuration.name + '.json')) {
                let jsonData = fs.readFileSync('./cache/' + configuration.name + '.json', 'utf-8');
                let json = JSON.parse(jsonData);

                for (let i = 0; i < json.length; i++) {
                    let preservation = json[i];

                    counter.inc(preservation.labels, preservation.value);
                }
            }
        } catch (e) {
            console.error(e);
        }

        return counter;
    }

    /**
     * Save the values of the application specific metrics to a file in cache/
     * @protected
     */
    _saveMetrics() {
        for (let metricName in this.metrics) {
            try {
                if (this.metrics.hasOwnProperty(metricName)) {
                    this.metrics['app_messages_received'].get().then(data => {
                        try {
                            fs.writeFileSync('cache/' + metricName + '.json', JSON.stringify(data.values));
                        } catch (e) {
                            console.error(e);
                        }
                    });
                }
            } catch (e) {
                console.error(e);
            }
        }
    }
}

module.exports = Webserver