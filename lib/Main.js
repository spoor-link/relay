const SubscriberManager = require('./SubscriberManager');
const PublisherManager = require('./PublisherManager');
const Webserver = require('./Webserver');
const fs = require('fs');
const YAML = require('yaml');

/**
 * Main application class. This class serves as a wrapper for all features of the app. It is initialized by executing this.run().
 */
class Main {
    /**
     * @type SubscriberManager
     */
    subscriberManager;

    /**
     * @type PublisherManager
     */
    publisherManager

    /**
     * @type Webserver
     */
    webserver;

    /**
     * @type string
     * File path of the configuration file
     */
    configFile;

    /**
     * The Constructor initializes its subscriber manager, publisher manager and webserver.
     */
    constructor() {
        this.subscriberManager = new SubscriberManager();
        this.publisherManager = new PublisherManager();
        this.webserver = new Webserver(this.subscriberManager, this.publisherManager);

        this.configFile = process.env.CONFIG_FILE || './config.yml';
    }

    /**
     * Starts the app.
     */
    run() {
        this._registerCallbacks();

        this._parseConfig(() => {
            this._watchConfig();
        });

        this.webserver.run();
    }

    /**
     * Registers all callbacks for the main functionality of the app. This enables communication between the app features
     * @protected
     */
    _registerCallbacks() {
        this.subscriberManager.subscriberCallback.register((err, address, envelope, message, publishers) => {
            this.publisherManager.send(envelope, message, publishers);
        });
    }

    /**
     * Configure the filesystem watcher on the config.yml file
     * @protected
     */
    _watchConfig() {
        fs.watchFile(this.configFile, () => {
            this._parseConfig();
        });

        let checkInterval = process.env.CONFIG_CHECK_INTERVAL ? parseInt(process.env.CONFIG_CHECK_INTERVAL) : 60000;

        if(checkInterval > 0) {
            setInterval(() => {
                this._parseConfig();
            }, Math.max(1000, checkInterval));
        }
    }

    /**
     * Processes a new config file. This takes the contents of the config.yml file, and orders all app features to update their configuration
     * @param {function|null} callback The function that gets called once the config file is processed
     * @protected
     */
    _parseConfig(callback=null) {
        fs.readFile(this.configFile, 'utf-8', (err, data) => {
            try {
                if (err)
                    console.error(err);

                let config = YAML.parse(data);

                this.subscriberManager.parseConfig(config.subscribers ?? config.clients);
                this.publisherManager.parseConfig(config.publishers ?? config.servers);
                this.webserver.parseConfig(config.prometheusEnvelopeMappers);
            } catch (e) {
                console.error(e);
            }

            if (callback)
                callback();
        });
    }
}

module.exports = Main;