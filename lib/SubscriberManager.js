const Callback = require('./Callback');
const SubscriberZeroMQ = require('./SubscriberZeroMQ');

/**
 * Tracks the clients and messages it receives from those clients.
 * Based on the configuration sent to the ClientManager, it will spawn 0 or more clients, which each connect to a server.
 */
class SubscriberManager {
    /**
     * JSON object of subscriber instances
     * @type {Object.<string, SubscriberZeroMQ>}
     */
    subscribers = {};

    /**
     * @type Callback
     */
    subscriberCallback;

    constructor() {
        this.subscriberCallback = new Callback();
    }

    /**
     * Receives a config object, and processes that into subscriber objects
     * @param {Object} subscribersConfig The configuration to apply
     */
    parseConfig(subscribersConfig) {
        try {
            let subscriberNames = [];

            // Only continue if the configuration holds an array
            if(subscribersConfig instanceof Array) {
                for (let i = 0; i < subscribersConfig.length; i++) {
                    try {
                        let subscriberYaml = subscribersConfig[i];

                        if(!subscriberYaml.publishers && subscriberYaml.servers)
                            subscriberYaml.publishers = subscriberYaml.servers;

                        // Ignore the entry if a variable is missing
                        if (!subscriberYaml.name || !subscriberYaml.address || !subscriberYaml.envelope || !subscriberYaml.publishers)
                            continue;

                        // Ignore the entry if the list of servers is invalid
                        if (!subscriberYaml.publishers instanceof Array)
                            continue;

                        subscriberYaml.minMessages = subscriberYaml.minMessages ?? null;
                        subscriberYaml.minMessagesCheckInterval = subscriberYaml.minMessagesCheckInterval ?? null;

                        subscriberNames.push(subscriberYaml.name);

                        // Match the configuration entry to an existing client
                        if (this.subscribers[subscriberYaml.name]) {
                            if (this.subscribers[subscriberYaml.name].address === subscriberYaml.address &&
                                this.subscribers[subscriberYaml.name].envelope === subscriberYaml.envelope &&
                                this.subscribers[subscriberYaml.name].minMessages === subscriberYaml.minMessages &&
                                this.subscribers[subscriberYaml.name].minMessagesCheckInterval === subscriberYaml.minMessagesCheckInterval) {
                                this.subscribers[subscriberYaml.name].publishers = subscriberYaml.publishers;

                                continue;
                            }

                            // If anything other than the list of publishers is changed, the subscriber has to be recreated
                            this.subscribers[subscriberYaml.name].destroy();
                            delete this.subscribers[subscriberYaml.name];
                        }

                        // Create a new client entry
                        try {
                            this.subscribers[subscriberYaml.name] = new SubscriberZeroMQ(
                                subscriberYaml.name,
                                subscriberYaml.address,
                                subscriberYaml.envelope,
                                subscriberYaml.publishers,
                                subscriberYaml.minMessages,
                                subscriberYaml.minMessagesCheckInterval
                            );
                            this.subscribers[subscriberYaml.name].messageCallback.register((err, address, envelope, message, publishers) => {
                                this._parseMessage(address, envelope, message, publishers);
                            });
                            this.subscribers[subscriberYaml.name].connect();
                        } catch (e) {
                            console.error(e);
                        }
                    } catch (e) {
                        console.error(e);
                    }
                }
            }

            // Delete any existing clients that don't exist in the new configuration
            for (let subscriber in this.subscribers) {
                if (this.subscribers.hasOwnProperty(subscriber) && !subscriberNames.includes(subscriber)) {
                    this.subscribers[subscriber].destroy();
                    delete this.subscribers[subscriber];
                }
            }
        } catch (e) {
            console.error(e);
        }
    }

    /**
     * Process a message received from a subscriber
     * @param {string} address The address of the server the message is received from
     * @param {string} envelope The envelope of the received message
     * @param {string} message The message received from the server
     * @param {string[]} publishers The publishers this message needs to be sent to
     * @protected
     */
    _parseMessage(address, envelope, message, publishers) {
        this.subscriberCallback.next(null, address, envelope, message, publishers);
    }
}

module.exports = SubscriberManager;