const PublisherZeroMQ = require('./PublisherZeroMQ');

/**
 * Tracks the publishers used to relay messages from outside publishers.
 * Based on the configuration sent to the PublisherManager, it will spawn 0 or more publishers.
 */
class PublisherManager {
    /**
     * JSON object of publisher instances
     * @type {Object.<string, PublisherZeroMQ>}
     */
    publishers = {};

    /**
     * Receives a config object, and processes that into publisher instances
     * @param {Object} publishersConfig The configuration to apply
     */
    parseConfig(publishersConfig) {
        try {
            let publisherNames = [];

            // Only continue if the configuration holds an array
            if(publishersConfig instanceof Array) {
                for (let i = 0; i < publishersConfig.length; i++) {
                    try {
                        let publisherYaml = publishersConfig[i];

                        // Ignore the entry if a variable is missing
                        if(!publisherYaml.name || !publisherYaml.binding)
                            continue;

                        publisherNames.push(publisherYaml.name);

                        // Match the configuration entry to an existing publisher
                        if (this.publishers[publisherYaml.name]) {
                            if (this.publishers[publisherYaml.name].binding === publisherYaml.binding)
                                continue;

                            this.publishers[publisherYaml.name].destroy();
                            delete this.publishers[publisherYaml.name];
                        }

                        // Create a new publisher entry
                        try {
                            this.publishers[publisherYaml.name] = new PublisherZeroMQ(
                                publisherYaml.name,
                                publisherYaml.binding,
                            );
                            this.publishers[publisherYaml.name].bind();
                        } catch (e) {
                            console.error(e);
                        }
                    } catch (e) {
                        console.error(e);
                    }
                }
            }

            // Delete any existing publishers that don't exist in the new configuration
            for (let publisher in this.publishers) {
                if (this.publishers.hasOwnProperty(publisher) && !publisherNames.includes(publisher)) {
                    this.publishers[publisher].destroy();
                    delete this.publishers[publisher];
                }
            }
        } catch (e) {
            console.error(e);
        }
    }

    /**
     * Send a message to the publishers, based on which publishers it should be sent to
     * @param {string} envelope The envelope of the message
     * @param {string} message The message itself
     * @param {string[]} publishers The list of publishers this message needs to be sent to
     */
    send(envelope, message, publishers) {
        try {
            for (let i = 0; i < publishers.length; i++) {
                if (this.publishers[publishers[i]]) {
                    this.publishers[publishers[i]].sendMessage(envelope, message);
                }
            }
        } catch (e) {
            console.error(e);
        }
    }
}

module.exports = PublisherManager;