const Callback = require('./Callback');
const zmq = require('zeromq');

/**
 * ZeroMQ Subscriber.
 * This connects to a ZeroMQ publisher, to receive its messages.
 */
class SubscriberZeroMQ {
    /**
     * @type string
     */
    name;

    /**
     * @type string
     */
    address;

    /**
     * @type string
     */
    envelope;

    minMessages = null;

    minMessagesCheckInterval = null;

    minMessagesCheckDuration = 0;

    checkIntervalId;

    messageCount = 0;

    /**
     * @type string[]
     */
    publishers;

    zmq;

    /**
     * @type Callback
     */
    messageCallback;

    /**
     * Initializes the subscriber object
     * @param {string} name The name of the connection (as human-readable string)
     * @param {string} address The TCP address of the server (example: tcp://localhost:7664)
     * @param {string} envelope The envelope of the messages to receive
     * @param {string[]} publishers A list of which servers messages from this subscriber need to be sent to
     * @param {?int} minMessages The minimum amount of messages that must be received within a timeframe. Restarts the subscriber if not met
     * @param {?string} minMessagesCheckInterval The interval in which to check the minimum amount of messages. Valid options are: second, minute or hour
     */
    constructor(name, address, envelope, publishers, minMessages = null, minMessagesCheckInterval = null) {
        this.name = name;
        this.address = address;
        this.envelope = envelope;
        this.publishers = publishers;
        this.messageCallback = new Callback();

        if (minMessages) {
            this.minMessages = minMessages;
            this.minMessagesCheckInterval = minMessagesCheckInterval;
            switch (minMessagesCheckInterval) {
                case 'second':
                case 'seconds':
                    this.minMessagesCheckDuration = 1000;
                    break;
                case 'hour':
                case 'hours':
                    this.minMessagesCheckDuration = 3600000;
                    break;
                default:
                    this.minMessagesCheckDuration = 60000;
                    break;
            }
        }
    }

    /**
     * Start the connection with the server
     */
    connect() {
        if (!this.zmq) {
            this.zmq = zmq.socket('sub');

            this.zmq.connect(this.address);
            this.zmq.subscribe(this.envelope);

            console.log('connected subscriber ' + this.name);

            this.zmq.on('message', (envelope, message) => {
                envelope = envelope.toString();

                this.messageCallback.next(null, this.address, envelope, message, this.publishers);

                if (this.minMessages)
                    this.messageCount++;
            });

            if (this.minMessages && !this.checkIntervalId) {
                console.log('Checking incoming messages for ' + this.name + ' every ' + this.minMessagesCheckDuration / 1000 + ' seconds');
                this.checkIntervalId = setInterval(() => {

                    this._checkMinMessages();
                }, this.minMessagesCheckDuration);
            }
        }
    }

    /**
     * Close the connection with the publisher
     */
    destroy() {
        if (this.zmq) {
            this.zmq.disconnect(this.address);

            console.log('disconnected subscriber ' + this.name);
        }

        if (this.checkIntervalId) {
            clearInterval(this.checkIntervalId);
            this.checkIntervalId = null;
        }
    }

    restart() {
        if (!this.zmq) {
            this.connect();
        } else {
            this.zmq.disconnect(this.address);
            this.zmq.connect(this.address);
            this.zmq.subscribe(this.envelope);
        }
    }

    _checkMinMessages() {
        // If the amount of messages does not meet the minimum criteria
        if (this.messageCount < this.minMessages) {
            console.log('Restarting subscriber ' + this.name + ' because of too few incoming messages');
            this.restart();
        }

        this.messageCount = 0;
    }
}

module.exports = SubscriberZeroMQ;