/**
 * benchmark.js
 *
 * Performs a benchmark of the relay. It hosts a ZeroMQ publisher and a ZeroMQ subscriber.
 * By sending messages to the relay from the publisher and listening for its response on the subscriber,
 * the benchmark can figure out the delay of the messages.
 *
 * To use this script, set the following environment variables:
 * PUBLISHER_ADDRESS: the address to which the publisher should bind (default: "tcp://0.0.0.0:7664")
 * SUBSCRIBER_ADDRESS: the address to which the subscriber should connect (the address of the relay)
 *
 * Connect your relay to the bind address of this benchmark. The benchmark can send a maximum of 1000 messages per
 * second to the relay.
 */

const zmq = require('zeromq');
const percentile = require("percentile");
const ss = require('simple-statistics');
const async = require("async");
const fs = require('fs');

// Set up publisher connection to send messages
let publisher = zmq.socket('pub');
publisher.bindSync(process.env.PUBLISHER_ADDRESS || 'tcp://0.0.0.0:7664');

// Set up subscriber connection to receive messages
let subscriber = zmq.socket('sub');
subscriber.connect(process.env.SUBSCRIBER_ADDRESS);
subscriber.subscribe('/');

/**
 * Store a list of all response times within a single test. Gets reset after each test
 * @type {number[]}
 */
let responseTimes = [];
/**
 * Counter of the amount of messages received within a single test. Gets reset after each test
 * @type {number}
 */
let messagesReceived = 0;
/**
 * List of JSON objects, each storing the results of a test, like minimum and maximum response times, etc...
 * @type {*[]}
 */
let results = [];

// When a message is received, process the message, and store the results in `responseTimes`
subscriber.on('message', (envelope, message) => {
    envelope = envelope.toString();

    if (envelope === '/BENCHMARK') { // If the message is part of the benchmark
        let receivedTime = parseInt(message);
        responseTimes.push(+new Date() - receivedTime);
        messagesReceived += 1;
    } else { // If the message is not part of the benchmark
        // Display the message, so the user can see there are other messages going through the relay
        console.log(envelope);
    }
});

/**
 * A list of JSON objects, each storing the test pattern
 * @type {*[]}
 */
let tests = [];

if (fs.existsSync('bench/test.json')) { // If a test.json file is present, use it for the test patterns
    let JSONString = fs.readFileSync('bench/test.json');
    tests = JSON.parse(JSONString);
} else { // Otherwise, use a pre-configured test pattern
    tests = [
        {
            messagesPerSecond: 1,
            testPeriodSeconds: 30,
        },
        {
            messagesPerSecond: 100,
            testPeriodSeconds: 30,
        },
        {
            messagesPerSecond: 1000,
            testPeriodSeconds: 30,
        },
        {
            messagesPerSecond: 1000,
            testPeriodSeconds: 60,
        },
    ];
}

// Before testing starts, the script waits 5 seconds for the connection to establish
console.log('Waiting 5 seconds for connection to establish');
setTimeout(() => {
    // Go through each test case
    async.eachSeries(tests, (test, callback) => {
        console.log(`Testing ${test.messagesPerSecond} messages a second for ${test.testPeriodSeconds} seconds`);

        stresstest(test.messagesPerSecond, test.testPeriodSeconds, () => {
            console.log('Test finished. Gathering results...');

            // Give the messages 1.5 seconds to be received by the client socket
            setTimeout(() => {
                console.log(`Number of messages: ${messagesReceived}`);

                // Only calculate the statistics if it has actually received any messages
                if (messagesReceived > 0) {
                    console.log(`Minimum time: ${ss.min(responseTimes)}`);
                    console.log(`Maximum time: ${ss.max(responseTimes)}`);
                    console.log(`Average time: ${ss.average(responseTimes)}`);
                    console.log(`Mean time: ${ss.mean(responseTimes)}`);
                    console.log(`Standard deviation: ${ss.standardDeviation(responseTimes)}`);
                    console.log(`90th percentile: ${percentile(90, responseTimes)}`);
                    console.log(`95th percentile: ${percentile(95, responseTimes)}`);
                    console.log(`99th percentile: ${percentile(99, responseTimes)}`);

                    // Store the results, to be pushed to a CSV file at the end
                    results.push({
                        messagesPerSecond: test.messagesPerSecond,
                        testPeriodSeconds: test.testPeriodSeconds,
                        messagesReceived,
                        min: ss.min(responseTimes),
                        max: ss.max(responseTimes),
                        average: ss.average(responseTimes),
                        mean: ss.mean(responseTimes),
                        standardDeviation: ss.standardDeviation(responseTimes),
                        '50thPercentile': percentile(50, responseTimes),
                        '90thPercentile': percentile(90, responseTimes),
                        '95thPercentile': percentile(95, responseTimes),
                        '99thPercentile': percentile(99, responseTimes),
                    });
                }
                console.log('-----------------');
                messagesReceived = 0;
                responseTimes = [];

                // Start the next test
                callback();
            }, 1500);
        });
    }, (err) => {
        if (err)
            console.error(err);

        /*
         * Store the results as a CSV file
         */
        if (!fs.existsSync('bench/')) {
            fs.mkdirSync('bench/');
        }

        let filename = `benchmark_${+new Date()}.csv`;
        let fileContents = 'msgPerSecond;period;msg;min;max;avg;mean;stdDev;50th;90th;95th;99th\r\n';

        for (let i = 0; i < results.length; i++) {
            let result = results[i];

            fileContents += `${result.messagesPerSecond};${result.testPeriodSeconds};${result.messagesReceived};${result.min};${result.max};${result.average};${result.mean};${result.standardDeviation};${result['50thPercentile']};${result['90thPercentile']};${result['95thPercentile']};${result['99thPercentile']}\r\n`;
        }

        try {
            fs.writeFileSync(`bench/${filename}`, fileContents);
        } catch (e) {
            console.error(e);

            // If the file fails to save, dump the results to the console
            console.log('');
            console.log(fileContents);
        }

        process.exit();
    });
}, 5000);

/**
 * Performs a stress test of the relay
 * @param {number} messagesPerSecond The amount of messages to be sent every second
 * @param {number} testPeriodSeconds The length of the stress test
 * @param {function} callback Gets called when the stress test finishes
 */
function stresstest(messagesPerSecond, testPeriodSeconds, callback) {
    messagesPerSecond = Math.min(1000, Math.max(1, messagesPerSecond));
    let messages = testPeriodSeconds * messagesPerSecond;

    let intervalId = setInterval(() => {
        publisher.send(['/BENCHMARK', +new Date()]);

        messages -= 1;
        if (messages <= 0) {
            clearInterval(intervalId);
            if (callback)
                callback();
        }
    }, 1000 / messagesPerSecond);
}