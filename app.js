/**
 * Main landing point of the app.
 * This file starts the Main class, which contains all the app logic.
 */

const Main = require('./lib/Main');

let app = new Main();
app.run();